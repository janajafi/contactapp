﻿using System;
using ContactApp;
using System.IO;
using NUnit.Framework;

namespace ContactAppUnitTestProject
{
    [TestFixture]
    public class ValidateInputUnitTests
    {
        private Validator _validator;

        [SetUp]
        public void Setup( )
        {
            _validator = new Validator();
        }

        [Test]
        public void RunApplicationWithoutProvingAnything_Validate_ThrowsInvalidPathException( )
        {
            var TestArray = new string[] { };

            Assert.That(( ) => _validator.ValidateFilePath(TestArray), Throws.Exception.TypeOf<Exception>());
        }


        [Test]
        public void ProvidingInvalidInputFileType_Validate_ThrowsInvalidFileTypeException( )
        {
            var TestArray = new string[] { "C:\\temp\\InputFiles\\names.xlsx" };

            Assert.That(( ) => _validator.ValidateFilePath(TestArray), Throws.Exception.TypeOf<InvalidFileTypeException>());

        }
        [Test]
        public void ProvidingInvalidPath_Validate_ThrowsInvalidPathException()
        {
            
            var TestArray = new string[] { "c>\\Inputfiles\\name.txt" };
            Assert.That(( ) => _validator.ValidateFilePath(TestArray), Throws.Exception.TypeOf<InvalidPathException>());
        
        }

        [Test]
        public void ProvidingFileDoesNotExist_Validate_ThrowsFileNotFoundException()
        {
           var TestArray = new string[] { Environment.CurrentDirectory + "\\Inputfiles\\" + "javidcontacts2015.txt" };
           Assert.That(( ) => _validator.ValidateFilePath(TestArray), Throws.Exception.TypeOf<FileNotFoundException>());
           
        }
    }
}
