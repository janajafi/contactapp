﻿using System;
using ContactApp;
using NUnit.Framework;

namespace ContactAppUnitTestProject
{
    [TestFixture]
    public class ValidateInputFileFormatUnitTest
    {
        private Validator _validator;

        [SetUp]
        public void Setup( )
        {
            _validator = new Validator();
        }

        [Test]
        public void ContactsAreNotSplittedByComma_Validate_ThrowsInvalidFileFormatException( )
        {
            string filepath = "C:\\temp\\InputFiles\\filewithoutcomma.txt";

            Assert.That(( ) => _validator.ValidateFileFormat(filepath), Throws.Exception.TypeOf<InvalidFileFormatException>());
        }

        [Test]
        public void ContactsAreSplittedByExtraComma_Validate_ThrowsInvalidFileFormatException( )
        {

            string filepath = "C:\\temp\\InputFiles\\FileWithTwoCommas.txt";

            Assert.That(( ) => _validator.ValidateFileFormat(filepath), Throws.Exception.TypeOf<InvalidFileFormatException>());
        }


        //[TestMethod]
        //public void ValidInputFile_Validate_TotalContactInsertedToContactList()
        //{
        //    ContactService cs = new ContactService();
        //    List<Contact> contacts;
        //    string filepath = Environment.CurrentDirectory + "\\Inputfiles\\" + "names.txt";
        //    contacts = cs.Validate(filepath);
        //    int actual = contacts.Count;
        //    int expected = 11;
        //    Assert.AreEqual(expected, actual);
        //}

    }
}
